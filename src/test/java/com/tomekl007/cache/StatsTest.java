package com.tomekl007.cache;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.CacheStats;
import com.google.common.cache.LoadingCache;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class StatsTest {
    @Test
    public void shouldGetStatsAboutCache() {
        final CacheLoader<String, String> loader = new CacheLoader<String, String>() {
            @Override
            public final String load(final String key) {
                return key.toUpperCase();
            }
        };
        final LoadingCache<String, String> cache = CacheBuilder
                .newBuilder()
                .maximumSize(3)
                .recordStats()
                .build(loader);
        cache.getUnchecked("first");
        cache.getUnchecked("second");
        cache.getUnchecked("third");
        for (int i = 0; i < 100; i++) {
            cache.getUnchecked("forth");
        }

        //when
        CacheStats stats = cache.stats();

        //then
        assertThat(stats.hitCount()).isEqualTo(99);
        assertThat(stats.missCount()).isEqualTo(4);
        assertThat(stats.loadSuccessCount()).isEqualTo(4);
        assertThat(stats.evictionCount()).isEqualTo(1);
        assertThat(cache.size()).isEqualTo(3);
    }
}
